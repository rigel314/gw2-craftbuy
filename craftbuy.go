package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.com/rigel314/gw2-craftbuy/treefinder"
)

var (
	id       = flag.Int("id", -1, "top level item ID")
	nocache  = flag.Bool("nocache", false, "disable reading cache, but still write cache")
	base     = flag.Bool("base", false, "show base ingredients")
	notree   = flag.Bool("notree", false, "don't show crafting tree")
	fullTree = flag.Bool("full", false, "show full crafting tree instead of price-optimized crafting tree")
	count    = flag.Int("count", 1, "top level desired count")
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	flag.Parse()

	if *id == -1 {
		log.Fatal("must provide -id, try 48075")
	}

	tf := treefinder.NewTreeFinder()

	if !*nocache {
		tf.LoadCache("cache")
	}

	thisTreeItemIDs, err := tf.CraftingTree(treefinder.ItemID(*id))
	if err != nil {
		log.Fatal(err)
	}
	prices, err := tf.ResolveItemsNamePrice(thisTreeItemIDs)
	if err != nil {
		log.Fatal(err)
	}
	tf.WriteCache("cache")

	if !*notree {
		if *fullTree {
			fmt.Println("Crafting tree: (shows * next to crafting recipes beyond the first)")
			tf.WriteTextTree(os.Stdout, treefinder.ItemID(*id), prices, *count)
		} else {
			fmt.Println("Price-optimized crafting tree: (shows cheapest crafting recipe, when more than one is available)")
			tf.WriteTextOptimizedTree(os.Stdout, treefinder.ItemID(*id), prices, *count)
		}
	}

	if *base {
		if *fullTree {
			fmt.Println("Base materials: (only looks at the first crafting recipe, when more than one is available)")
			tf.WriteTextBase(os.Stdout, treefinder.ItemID(*id), prices, *count)
		} else {
			fmt.Println("Price-optimized base materials: (only looks at the cheapest crafting recipe, when more than one is available)")
			tf.WriteTextOptimizedBase(os.Stdout, treefinder.ItemID(*id), prices, *count)
		}
	}
}
