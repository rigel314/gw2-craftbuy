module gitlab.com/rigel314/gw2-craftbuy

go 1.17

require gitlab.com/MrGunflame/gw2api v1.0.3

replace gitlab.com/MrGunflame/gw2api => gitlab.com/rigel314/gw2api v1.0.4-0.20220417182016-5f6e09195071

// replace gitlab.com/MrGunflame/gw2api => ../gw2api
