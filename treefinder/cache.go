package treefinder

import (
	"compress/gzip"
	"encoding/gob"
	"log"
	"os"
)

func (tf *TreeFinder) LoadCache(path string) {
	fail := true
	defer func() {
		if fail {
			log.Println("problem with gw2 api cache, ignoring cache")
		}
	}()

	f, err := os.Open(path)
	if err != nil {
		return
	}
	defer f.Close()

	grd, err := gzip.NewReader(f)
	if err != nil {
		return
	}
	defer grd.Close()

	dec := gob.NewDecoder(grd)
	err = dec.Decode(&tf.items)
	if err != nil {
		return
	}
	err = dec.Decode(&tf.itemRecps)
	if err != nil {
		return
	}
	err = dec.Decode(&tf.recps)
	if err != nil {
		return
	}

	fail = false
}

func (tf *TreeFinder) WriteCache(path string) {
	if !tf.dirty {
		return
	}

	fail := true
	defer func() {
		if fail {
			log.Println("problem writing cache")
		}
	}()

	f, err := os.Create(path)
	if err != nil {
		return
	}
	defer f.Close()

	gwr := gzip.NewWriter(f)
	defer gwr.Close()

	enc := gob.NewEncoder(gwr)
	err = enc.Encode(tf.items)
	if err != nil {
		return
	}
	err = enc.Encode(tf.itemRecps)
	if err != nil {
		return
	}
	err = enc.Encode(tf.recps)
	if err != nil {
		return
	}

	fail = false
}
