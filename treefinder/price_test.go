package treefinder

import (
	"math"
	"testing"
)

func TestPrice(t *testing.T) {
	c := price(5)
	if c.String() != "5c" {
		t.Log("failed copper string:", c.String())
		t.Fail()
	}
	s := price(500)
	if s.String() != "5s0c" {
		t.Log("failed silver string:", s.String())
		t.Fail()
	}
	g := price(50000)
	if g.String() != "5g0s0c" {
		t.Log("failed gold string:", g.String())
		t.Fail()
	}
}

func TestNoPrice(t *testing.T) {
	zero := price(0)
	if zero.String() != "no-TP" {
		t.Log("failed zero string:", zero.String())
		t.Fail()
	}
	neg1 := price(-1)
	if neg1.String() != "no-TP" {
		t.Log("failed neg1 string:", neg1.String())
		t.Fail()
	}
	neg2 := price(-2)
	if neg2.String() != "no-TP" {
		t.Log("failed neg2 string:", neg2.String())
		t.Fail()
	}
	max := price(math.MaxInt)
	if max.String() != "no-TP" {
		t.Log("failed max string:", max.String())
		t.Fail()
	}
}
