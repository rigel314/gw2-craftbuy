package treefinder

import (
	"fmt"
	"math"

	"gitlab.com/MrGunflame/gw2api"
)

type price int

func (p price) String() string {
	if !p.Tradepostable() {
		return "no-TP"
	}
	g := p / 100 / 100
	s := (p / 100) % 100
	c := p % 100
	if g != 0 {
		return fmt.Sprintf("%dg%ds%dc", g, s, c)
	}
	if s != 0 {
		return fmt.Sprintf("%ds%dc", s, c)
	}
	return fmt.Sprintf("%dc", c)
}

func (p price) Tradepostable() bool {
	return !(p == math.MaxInt || p <= 0)
}

func (p price) Sub(o price) priceDiff {
	if p.Tradepostable() && o.Tradepostable() {
		return priceDiff(p - o)
	}

	return priceDiff(math.MaxInt)
}

func (p price) Div(o price) price {
	if p.Tradepostable() && o.Tradepostable() {
		return p / o
	}

	return price(math.MaxInt)
}

type priceDiff int

func (pd priceDiff) String() string {
	if pd == 0 {
		return "0c"
	}
	return price(pd).String()
}

type Prices map[ItemID]*gw2api.CommercePrice
