package treefinder

import (
	"fmt"
	"io"
	"math"
	"sort"
	"unsafe"

	"gitlab.com/MrGunflame/gw2api"
)

type RecipeID int
type ItemID int

type TreeFinder struct {
	api       *gw2api.Session
	items     map[ItemID]*gw2api.Item
	itemRecps map[ItemID][]RecipeID
	recps     map[RecipeID]*gw2api.Recipe
	dirty     bool
}

func NewTreeFinder() *TreeFinder {
	return &TreeFinder{
		api:       gw2api.New(),
		items:     make(map[ItemID]*gw2api.Item),
		itemRecps: make(map[ItemID][]RecipeID),
		recps:     make(map[RecipeID]*gw2api.Recipe),
	}
}

func (tf *TreeFinder) CraftingTree(id ItemID) (map[ItemID]struct{}, error) {
	thisTreeItemIDs := make(map[ItemID]struct{})
	return thisTreeItemIDs, tf.craftingTree(id, thisTreeItemIDs)
}

func (tf *TreeFinder) craftingTree(id ItemID, thisTreeItemIDs map[ItemID]struct{}) error {
	thisTreeItemIDs[id] = struct{}{}

	useAPI := true

	if _, ok := tf.itemRecps[id]; ok {
		useAPI = false
	}

	var rs []*gw2api.Recipe

	if useAPI {
		tf.dirty = true
		recpIds, err := tf.api.RecipesSearchOutput(int(id))
		if err != nil {
			return err
		}

		if len(recpIds) == 0 {
			tf.itemRecps[id] = nil
			return nil
		}
		tf.itemRecps[id] = unsafe.Slice((*RecipeID)(unsafe.Pointer(&recpIds[0])), len(recpIds))

		rs, err = tf.api.Recipes(recpIds...)
		if err != nil {
			return err
		}
	} else {
		for _, v := range tf.itemRecps[id] {
			rs = append(rs, tf.recps[v])
		}
	}

	for _, v := range rs {
		if useAPI {
			tf.recps[RecipeID(v.ID)] = v
		}
		for _, v := range v.Ingredients {
			err := tf.craftingTree(ItemID(v.ItemID), thisTreeItemIDs)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (tf *TreeFinder) ResolveItemsNamePrice(thisTreeItemIDs map[ItemID]struct{}) (map[ItemID]*gw2api.CommercePrice, error) {
	m := make(map[ItemID]struct{}) // set of itemIDs

	for k := range tf.itemRecps { // start with all the itemids in itemRecps
		m[k] = struct{}{}
	}
	for k := range tf.items { // remove all the itemids we have cached
		delete(m, k)
	}
	ids := make([]int, 0, len(m))
	for k := range m {
		ids = append(ids, int(k))
	}

	if len(ids) > 0 { // only fetch any item details if we don't already know about all the ids
		tf.dirty = true
		itms, err := tf.api.Items(ids...)
		if err != nil {
			return nil, err
		}
		for _, v := range itms {
			tf.items[ItemID(v.ID)] = v
		}
	}

	tpIds := make([]int, 0, len(thisTreeItemIDs))
	for k := range thisTreeItemIDs {
		tpIds = append(tpIds, int(k))
	}
	// log.Println(tpIds)
	// log.Println(thisTreeItemIDs)
	prices, err := tf.api.CommercePrices(tpIds...)
	if err != nil {
		if _, ok := err.(*gw2api.Error); !ok { // Ignore CommercePrices API saying "all the ids are not tradepost-able", or any other successful http request that returned an API error
			return nil, err
		}
	}
	priceMap := make(map[ItemID]*gw2api.CommercePrice)
	for _, v := range tpIds {
		priceMap[ItemID(v)] = &gw2api.CommercePrice{ID: v}
		priceMap[ItemID(v)].Sells.UnitPrice = math.MaxInt // Initialize sell price to max int, so something non-tradepost-able always compares really expensive
	}
	for _, v := range prices {
		priceMap[ItemID(v.ID)] = v
	}

	return priceMap, nil
}

func (tf *TreeFinder) WriteTextTree(wr io.Writer, id ItemID, prices Prices, count int) {
	tf.writeTextTree(wr, id, prices, count, 0, 0)
}

func (tf *TreeFinder) writeTextTree(wr io.Writer, id ItemID, prices Prices, count, depth, rcount int) {
	var indent string
	for i := 0; i < depth; i++ {
		indent += "\t"
	}
	for i := 0; i < rcount; i++ {
		indent += "*"
	}
	// log.Println(id, tf.itemRecps[id])
	tf.printItem(wr, id, count, price(prices[id].Sells.UnitPrice), nil, indent)
	for i, v := range tf.itemRecps[id] { // for each crafting recipe that makes this item
		recp := tf.recps[v]
		// log.Println(id, v, tf.recps[v].Ingredients)
		for _, v := range recp.Ingredients { // for each ingredient of this recipe
			tf.writeTextTree(wr, ItemID(v.ItemID), prices, v.Count, depth+1, i)
		}
	}
}

func (tf *TreeFinder) WriteTextBase(wr io.Writer, id ItemID, prices Prices, count int) {
	itemCounts := make(map[ItemID]int)
	tf.calculateBase(wr, id, prices, count, itemCounts)
	keys := make([]int, 0, len(itemCounts))
	for k := range itemCounts {
		keys = append(keys, int(k))
	}
	sort.Ints(keys)
	total := 0
	for _, key := range keys {
		k := ItemID(key)
		v := itemCounts[k]
		p := price(prices[k].Sells.UnitPrice)
		tf.printItem(wr, k, v, p, nil, "")
		if p.Tradepostable() {
			total += prices[k].Sells.UnitPrice * v
		}
	}
	fmt.Fprintf(wr, "%v total\n", price(total))
}

func (tf *TreeFinder) calculateBase(wr io.Writer, id ItemID, prices Prices, count int, itemCounts map[ItemID]int) {
	if len(tf.itemRecps[id]) == 0 {
		itemCounts[id] += count
		return
	}

	// only look at the first recipe
	recp := tf.recps[tf.itemRecps[id][0]]
	for _, v := range recp.Ingredients { // for each ingredient of this recipe
		tf.calculateBase(wr, ItemID(v.ItemID), prices, v.Count*count/recp.OutputItemCount, itemCounts)
	}
}

// returns the minimum price of the item and a bool representing if it is the item's direct price, or the price of the ingredients
func (tf *TreeFinder) treePrice(id ItemID, prices Prices, count int) (price, bool, price) {
	itemPrice := price(prices[id].Sells.UnitPrice)

	if itemPrice.Tradepostable() {
		itemPrice *= price(count)
	}

	if len(tf.itemRecps[id]) == 0 { // if this item has no recipes
		return itemPrice, true, itemPrice
	}

	min := price(math.MaxInt)
	for _, v := range tf.itemRecps[id] {
		recp := tf.recps[v]
		total := price(0)
		for _, v := range recp.Ingredients {
			p, _, _ := tf.treePrice(ItemID(v.ItemID), prices, v.Count)
			if p.Tradepostable() {
				total += p * price(count)
			}
		}
		total /= price(recp.OutputItemCount)
		if total < min {
			min = total
		}
	}

	if min < itemPrice {
		return min, false, itemPrice
	}

	return itemPrice, true, min
}

func (tf *TreeFinder) WriteTextOptimizedTree(wr io.Writer, id ItemID, prices Prices, count int) {
	tf.writeTextOptimizedTree(wr, id, prices, count, 0)
}

func (tf *TreeFinder) writeTextOptimizedTree(wr io.Writer, id ItemID, prices Prices, count, depth int) {
	var indent string
	for i := 0; i < depth; i++ {
		indent += "\t"
	}
	p, isItem, otherP := tf.treePrice(id, prices, 1)
	tf.printItem(wr, id, count, p, &otherP, indent)
	if !isItem {
		min := price(math.MaxInt)
		minIdx := 0
		for i, v := range tf.itemRecps[id] { // for each crafting recipe that makes this item
			var recpP price
			recp := tf.recps[v]
			for _, v := range recp.Ingredients { // for each ingredient of this recipe
				p, _, _ := tf.treePrice(ItemID(v.ItemID), prices, v.Count)
				if p.Tradepostable() {
					recpP += p
				}
			}
			recpP /= price(recp.OutputItemCount)
			if recpP.Tradepostable() && recpP < min {
				minIdx = i
			}
		}
		for _, v := range tf.recps[tf.itemRecps[id][minIdx]].Ingredients { // for each ingredient of this recipe
			tf.writeTextOptimizedTree(wr, ItemID(v.ItemID), prices, v.Count, depth+1)
		}
	}
}

func (tf *TreeFinder) WriteTextOptimizedBase(wr io.Writer, id ItemID, prices Prices, count int) {
	itemCounts := make(map[ItemID]int)
	tf.calculateOptimizedBase(wr, id, prices, count, itemCounts)
	keys := make([]int, 0, len(itemCounts))
	for k := range itemCounts {
		keys = append(keys, int(k))
	}
	sort.Ints(keys)
	total := 0
	for _, key := range keys {
		k := ItemID(key)
		v := itemCounts[k]
		p, _, otherP := tf.treePrice(k, prices, 1)
		tf.printItem(wr, k, v, p, &otherP, "")
		if p.Tradepostable() {
			total += prices[k].Sells.UnitPrice * v
		}
	}
	fmt.Fprintf(wr, "%v total\n", price(total))
}

func (tf *TreeFinder) calculateOptimizedBase(wr io.Writer, id ItemID, prices Prices, count int, itemCounts map[ItemID]int) {
	_, isItem, _ := tf.treePrice(id, prices, count)
	if isItem {
		itemCounts[id] += count
		return
	}

	min := price(math.MaxInt)
	minIdx := 0
	for i, v := range tf.itemRecps[id] { // for each crafting recipe that makes this item
		var recpP price
		recp := tf.recps[v]
		for _, v := range recp.Ingredients { // for each ingredient of this recipe
			p, _, _ := tf.treePrice(ItemID(v.ItemID), prices, v.Count)
			if p.Tradepostable() {
				recpP += p
			}
		}
		recpP /= price(recp.OutputItemCount)
		if recpP.Tradepostable() && recpP < min {
			minIdx = i
		}
	}
	bestRecpId := tf.itemRecps[id][minIdx]
	recp := tf.recps[bestRecpId]
	for _, v := range recp.Ingredients { // for each ingredient of this recipe
		tf.calculateOptimizedBase(wr, ItemID(v.ItemID), prices, v.Count*count/recp.OutputItemCount, itemCounts)
	}
}

func (tf *TreeFinder) printItem(wr io.Writer, id ItemID, count int, p price, otherP *price, indent string) {
	if p.Tradepostable() && (otherP != nil && otherP.Tradepostable()) {
		fmt.Fprintf(wr, "%s% 5dx %s [%d] (%v/ea, %v total, %v savings)\n", indent, count, tf.items[id].Name, id, p, p*price(count), otherP.Sub(p)*priceDiff(count))
		return
	}
	if p.Tradepostable() && (otherP == nil || !otherP.Tradepostable()) {
		fmt.Fprintf(wr, "%s% 5dx %s [%d] (%v/ea, %v total)\n", indent, count, tf.items[id].Name, id, p, p*price(count))
		return
	}

	fmt.Fprintf(wr, "%s% 5dx %s [%d] (no Tradepost)\n", indent, count, tf.items[id].Name, id)
}
